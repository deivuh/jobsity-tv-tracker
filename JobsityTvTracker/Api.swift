//
//  Api.swift
//  JobsityTvTracker
//
//  Created by David Hsieh on 12/11/21.
//

import Foundation


class Api {
    
    func fetchShow(completion:@escaping (Show) -> ()) {
        guard let url = URL(string: "https://api.tvmaze.com/shows") else {
            print("Failed to connect to API to retrieve show")
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            do {
                let show = try JSONDecoder().decode(Show.self, from: data!)
//                print(shows)
                DispatchQueue.main.async {
                    completion(show)
                }
            } catch {
                print("Failed to decode show index JSON: \(error)")
            }
            
            
        }.resume()
    }
    
    func fetchShows(completion:@escaping ([Show]) -> ()) {
        guard let url = URL(string: "https://api.tvmaze.com/shows") else {
            print("Failed to connect to API to retrieve shows index")
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            do {
                let shows = try JSONDecoder().decode([Show].self, from: data!)
//                print(shows)
                DispatchQueue.main.async {
                    completion(shows)
                }
            } catch {
                print("Failed to decode show index JSON: \(error)")
            }
            
            
        }.resume()
    }
    
    func fetchShows(page: Int, completion:@escaping ([Show]) -> ()) {
        guard let url = URL(string: "https://api.tvmaze.com/shows?page=\(page)") else {
            print("Failed to connect to API to retreive shows index, page \(page)")
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            do {
                let shows = try JSONDecoder().decode([Show].self, from: data!)
//                print(shows)
                DispatchQueue.main.async {
                    completion(shows)
                }
            } catch {
                print("Failed to decode shows index page \(page) JSON: \(error)")
            }
            
            
        }.resume()
    }
    
    func fetchShows(query: String, completion:@escaping ([SearchResult]) -> ()) {
        guard let url = URL(string: "https://api.tvmaze.com/search/shows?q=\(query)") else {
            print("Failed to connect to API to search for \(query)")
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            print()
            
            do {
                let results = try JSONDecoder().decode([SearchResult].self, from: data!)
                print(results)
                
                DispatchQueue.main.async {
                    completion(results)
                }
            } catch {
                
                print("Failed to decode queried shows index JSON: \(error)")
            }
            
            
        }.resume()
    }
    
    func fetchSeasons(showId: Int, completion:@escaping ([Season]) -> ()) {
        guard let url = URL(string: "https://api.tvmaze.com/shows/\(showId)/seasons") else {
            print("Failed to connect to API to retreive show seasons")
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            do {
                let seasons = try JSONDecoder().decode([Season].self, from: data!)
//                print(seasons)
                DispatchQueue.main.async {
                    completion(seasons)
                }
            } catch {
                print("Failed to decode Seasons JSON: \(error)")
            }
            
            
        }.resume()
    }
    
    func fetchEpisodes(seasonId: Int, completion:@escaping ([Episode]) -> ()) {
        guard let url = URL(string: "https://api.tvmaze.com/seasons/\(seasonId)/episodes") else {
            print("Failed to connect to API to retreive season episodes")
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            do {
                let episodes = try JSONDecoder().decode([Episode].self, from: data!)
//                print(episodes)
                DispatchQueue.main.async {
                    completion(episodes)
                }
            } catch {
                print("Failed to decode episodes JSON: \(error)")
            }
            
            
        }.resume()
    }
    
    
    func fetchPeople(page: Int, completion:@escaping ([Person]) -> ()) {
        guard let url = URL(string: "https://api.tvmaze.com/people?page=\(page)") else {
            print("Failed to connect to API to retreive persons index, page \(page)")
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in

            do {
                let people = try JSONDecoder().decode([Person].self, from: data!)

                DispatchQueue.main.async {
                    completion(people)
                }
            } catch {
                print("Failed to decode people index page: \(error)")
            }
            

        }.resume()
    }
    
}


