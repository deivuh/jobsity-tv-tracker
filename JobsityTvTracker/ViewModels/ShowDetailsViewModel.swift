//
//  ShowParser.swift
//  JobsityTvTracker
//
//  Created by David Hsieh on 13/11/21.
//

import Foundation



protocol ShowDetailsViewModelDelegate: AnyObject {
    func didLoadData(seasons: [Season])
    func willLoadData()
}

protocol ShowDetailsDelegate {
    func getSeasons(showId: Int)
    func isFavorite(showId: Int) -> Bool
    func setFavorite(showId: Int)
    var delegate: ShowDetailsViewModelDelegate? { get set }
}

extension ShowDetailsViewController: ShowDetailsViewModelDelegate {
    
    
    func didLoadData(seasons: [Season]) {
        
        self.currentShow?.seasons = seasons
        
        self.tableView.reloadData()
//        Activity Indicator off
        print(self.currentShow ?? "")
        
        self.hideActivityIndicator()
    }
    

    
    func willLoadData() {
        self.showActivityIndicator()
    }
    
    func showActivityIndicator() {
        
        self.activityView.isHidden = false
        self.activityView.startAnimating()
   }
    
    func hideActivityIndicator() {
        self.activityView.stopAnimating()
        self.activityView.isHidden = true
        
    }
    
    
}


class ShowDetailsViewModel: ShowDetailsDelegate {

    
    
    
    
    internal weak var delegate: ShowDetailsViewModelDelegate?
    
    func setFavorite(showId: Int) {
        let userDefaults = UserDefaults.standard
        
        var favorites = [Int]()
                
        
        
        if (userDefaults.value(forKey: "favorites") != nil) {
            favorites = userDefaults.value(forKey: "favorites") as? [Int] ?? [Int]()
        }
        
        if favorites.contains(showId) {
            if let index = favorites.firstIndex(of: showId) {
              favorites.remove(at: index)
              
            }
        } else {
            favorites.append(showId)
            
        }
        
        
        
        userDefaults.set(favorites, forKey: "favorites")
    }
    
    func save(show: Show) {
        
        
        var favorites = loadAndDecodeFavorites()
                
                      
        if favorites.contains(show) {
            if let index = favorites.firstIndex(of: show) {
              favorites.remove(at: index)
              
            }
        } else {
            favorites.append(show)
            
        }
        
        encodeAndSave(favorites: favorites)
        
        
    }
    
    func encodeAndSave(favorites: [Show]) {
        // Encode to save to UserDefaults
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(favorites) {
            let userDefaults = UserDefaults.standard
            userDefaults.set(encoded, forKey: "SavedShows")
        }
    }
    
    func loadAndDecodeFavorites() -> [Show] {
        let userDefaults = UserDefaults.standard
        if let savedFavorites = userDefaults.object(forKey: "SavedShows") as? Data {
            let decoder = JSONDecoder()
            if let loadedFavorites = try? decoder.decode([Show].self, from: savedFavorites) {
                return loadedFavorites
            }
        }
        return [Show]()
    }
    
    func isFavorite(showId: Int) -> Bool {
        let userDefaults = UserDefaults.standard
        let favorites = userDefaults.object(forKey: "favorites") as? [Int] ?? [Int]()
        return favorites.contains(showId)
        
    }
    
    func isSaved(show: Show) -> Bool {
        
        return loadAndDecodeFavorites().contains(show)
        
    }
    
    func getSeasons(showId: Int) {
        self.delegate?.willLoadData()
        Api().fetchSeasons(showId: showId, completion: { seasons in
            
            var seasons = seasons

            
            var seasonsCounter = 0
            
            for i in 0..<seasons.count {
                Api().fetchEpisodes(seasonId: seasons[i].id, completion: { episodes in
                    seasons[i].episodes = episodes
                    
                    // Add 1 after completion of each season's episodes request
                    seasonsCounter += 1
                    
                    
                    print("Season \(seasons[i].number)")
                    print(episodes.map { $0.number ?? -1 } )
                    
                    
                    if seasonsCounter >= seasons.count { // If every season's episode fetch has completed
                        
                        self.delegate?.didLoadData(seasons: seasons)
                    }
                })
            }
            
            
        })
        
    }
    
    
    
    
}
