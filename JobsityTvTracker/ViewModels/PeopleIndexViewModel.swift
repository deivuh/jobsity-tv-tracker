//
//  PeopleIndexViewModel.swift
//  JobsityTvTracker
//
//  Created by David Hsieh on 15/11/21.
//

import Foundation



protocol PeopleIndexViewModelDelegate: AnyObject {
    func didLoadData(people: [Person])
    func willLoadData()
}


protocol PeopleIndexDelegate {
    func getPeopleIndex()
    func getPeopleOn(page: Int)
    var delegate: PeopleIndexViewModelDelegate? { get set }
}

extension PeopleIndexViewController: PeopleIndexViewModelDelegate {
    
    func didLoadData(people: [Person]) {
        
        self.people = people
        
        self.hideActivityIndicator()
        self.tableView.reloadData()
        
    }
    
    
    
    func willLoadData() {
        
        showActivityIndicator()
        
    }
    
    func showActivityIndicator() {

        self.activityView.isHidden = false
        self.activityView.startAnimating()
   }

    func hideActivityIndicator() {
        self.activityView.stopAnimating()
        self.activityView.isHidden = true

    }
    
}


class PeopleIndexViewModel: PeopleIndexDelegate {

    var delegate: PeopleIndexViewModelDelegate?
    
    
    func getPeopleIndex() {
        self.delegate?.willLoadData()
        Api().fetchPeople(page: 0, completion: { (people) in
            self.delegate?.didLoadData(people: people)
            
        })
    }
    
    func getPeopleOn(page: Int) {
        self.delegate?.willLoadData()
        Api().fetchPeople(page: page, completion: { people in
            self.delegate?.didLoadData(people: people)
            
        })
    }
    

   
    
    
        
    
    
}


