//
//  ShowsIndexViewModel.swift
//  JobsityTvTracker
//
//  Created by David Hsieh on 14/11/21.
//

import Foundation
import UIKit

protocol ShowsIndexViewModelDelegate: AnyObject {
    func didLoadData(shows: [Show])
    func willLoadData()
}


protocol ShowsIndexDelegate {
    func searchForShow(query: String)
    func getShowsIndex()
    func getFavoriteShows()
    func getShowsOn(page: Int)
    var delegate: ShowsIndexViewModelDelegate? { get set }
}

extension ShowsIndexViewController: ShowsIndexViewModelDelegate {
    
    func didLoadData(shows: [Show]) {
        
        if self.segmentedControl.selectedSegmentIndex == 0 && self.nextPage > 0{
            
            self.shows.append(contentsOf: shows)
        } else {
            self.shows = shows
        }
        
    
        
        self.isLoading = false
        
        if nextPage > 0 && self.segmentedControl.selectedSegmentIndex == 0{

            guard let visibleRows = self.tableView.indexPathsForVisibleRows else {
                return
            }
            self.tableView.scrollToRow(at: visibleRows.last ?? IndexPath(), at: .top, animated: true)

        }
        self.nextPage += 1
        
        self.hideActivityIndicator()
        self.tableView.reloadData()
        
    }
    
    
    
    func willLoadData() {
        
        showActivityIndicator()
        self.isLoading = true
    }
    
    func showActivityIndicator() {
        
        self.activityView.isHidden = false
        self.activityView.startAnimating()
   }
    
    func hideActivityIndicator() {
        self.activityView.stopAnimating()
        self.activityView.isHidden = true
        
    }
    
}


class ShowsIndexViewModel: ShowsIndexDelegate {
    
    internal weak var delegate: ShowsIndexViewModelDelegate?
    
    func getFavoriteShows() {
        self.delegate?.willLoadData()
        let userDefaults = UserDefaults.standard
        if userDefaults.object(forKey: "SavedShows") == nil {
            if let encoded = try? JSONEncoder().encode([Show]()) {
                let userDefaults = UserDefaults.standard
                userDefaults.set(encoded, forKey: "SavedShows")
            }
        }
        if let savedFavorites = userDefaults.object(forKey: "SavedShows") as? Data {
            let decoder = JSONDecoder()
            if let loadedFavorites = try? decoder.decode([Show].self, from: savedFavorites) {
                self.delegate?.didLoadData(shows: loadedFavorites.sorted(by: {$0.name < $1.name} ))
         
            }
        } else { }
        
    }
    
    
    func getShowsIndex() {
        self.delegate?.willLoadData()
        Api().fetchShows { (shows) in
            self.delegate?.didLoadData(shows: shows)
            
        }
    }
    
    func getShowsOn(page: Int) {
        self.delegate?.willLoadData()
        Api().fetchShows(page: page, completion: { shows in
            self.delegate?.didLoadData(shows: shows)
            
        })
    }
    

   
    
    
        
    func searchForShow(query: String) {
        
        self.delegate?.willLoadData()
        Api().fetchShows(query: query, completion: { results in
            
            if !results.isEmpty {
                
                let sortedResults = results.sorted { $0.score ?? 0 > $1.score ?? 0 } // Sort results array by score, eve nif it's sorted by default, just in case
                
                let sortedShows = sortedResults.map { $0.show }
                self.delegate?.didLoadData(shows: sortedShows)
            } else {
                self.delegate?.didLoadData(shows: [Show]())
            }
        })
    }
    
}


