//
//  Shows.swift
//  JobsityTvTracker
//
//  Created by David Hsieh on 12/11/21.
//


import Foundation

struct Show: Codable, Identifiable, Equatable {
    let id: Int
    var name: String
    var genres: [String]
    
    
    struct Image: Codable {
        var medium: String
        var original: String
    }
        
    struct Schedule: Codable {
        var time: String?
        var days: [String]?
    }
    
    var imageUrls: Image?
    var schedule: Schedule?
    
    var seasons: [Season]?
    
    var summary: String?
    
    var formattedSummary: String?
    
    var premiered: String?
    
    private enum CodingKeys: String, CodingKey {
        case id, name, genres, imageUrls = "image", schedule, summary, premiered

        
    }
    
    // Compare id when comparing objects
    static func ==(lhs: Show, rhs: Show) -> Bool {
        return lhs.id == rhs.id
    }

        
}

struct Season: Codable, Identifiable {
    let id: Int
    var number: Int
    var episodes: [Episode]?
    
    
    private enum CodingKeys: String, CodingKey {
        case id, number
    }
}



struct Episode: Codable {
    let id: Int
    var name: String?
    var number: Int?
    var season: Int
    var summary: String?
    struct Image: Codable {
        var medium: String?
        var original: String?
    }
    var image: Image?

}

struct SearchResult: Codable {
    var show: Show
    let score: Float?
}


struct Person: Codable, Identifiable {
    let id: Int
    var name: String
    
    
    struct Image: Codable {
        var medium: String
        var original: String
    }
        
    var imageUrls: Image?
    
    private enum CodingKeys: String, CodingKey {
        case id, name, imageUrls = "image"

        
    }
    
   

        
}
