//
//  ShowTitleTableviewCell.swift
//  JobsityTvTracker
//
//  Created by David Hsieh on 12/11/21.
//

import UIKit
import SDWebImage

class ShowTitleTableviewCell: UITableViewCell {
    
    @IBOutlet weak var showName: UILabel!
    @IBOutlet weak var showImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
