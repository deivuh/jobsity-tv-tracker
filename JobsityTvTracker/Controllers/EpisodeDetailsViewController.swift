//
//  EpisodeDetailsViewController.swift
//  JobsityTvTracker
//
//  Created by David Hsieh on 14/11/21.
//

import UIKit
import SDWebImage

class EpisodeDetailsViewController: UITableViewController {
    
    var episode: Episode?

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let episode = self.episode else {
            return
        }
        
        print(episode)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        let cell = UITableViewCell.init(style: .subtitle, reuseIdentifier: nil)
        
        guard let episode = self.episode else {
            return cell
        }
        
        switch (indexPath.row) {
        case 0:
            cell.detailTextLabel?.text = "Episode \(episode.season)x\(episode.number ?? 0)"
            cell.textLabel?.text = episode.name
            return cell
        case 1:
            let cell = UITableViewCell.init(style: .default, reuseIdentifier: nil)
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.attributedText = episode.summary?.htmlAttributedString()
            cell.textLabel?.font = UIFont(name: "Helvetica Neue", size: 13.5)
            cell.textLabel?.sizeToFit()
            cell.textLabel?.textColor = .label
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell") as! ImageCell
            cell.episodeImageView.sd_setImage(with: URL(string: episode.image?.original ?? ""))
            cell.episodeImageView.sizeToFit()
            return cell
            
        default:
            return cell
        }
        

    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 2:
            
            return 200
            
        default:
            return UITableView.automaticDimension
        }
        
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
