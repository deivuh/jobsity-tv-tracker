//
//  ShowDetailsViewController.swift
//  JobsityTvTracker
//
//  Created by David Hsieh on 13/11/21.
//

import UIKit
import SDWebImage


class ShowDetailsViewController: UITableViewController {


    
    var currentShow: Show?
    var viewModel = ShowDetailsViewModel()
    let activityView = UIActivityIndicatorView(style: .large)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let currentShow = currentShow else {
            print("Not Show object was passed from previous VC")
            return
        }
        
        self.activityView.center = CGPoint(x: self.view.center.x, y: self.view.center.y-100)
        self.view.addSubview(self.activityView)        
        
        self.viewModel.delegate = self
        self.viewModel.getSeasons(showId: currentShow.id)
        
        
        self.title = currentShow.name
        
       refreshFavStatus()
        
        
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    func refreshFavStatus() {
        
        guard let currentShow = currentShow else {
            print("Not Show object was passed from previous VC")
            return
        }
        
//        let favImage = self.viewModel.isFavorite(showId: currentShow.id) ? UIImage(systemName: "heart.fill") : UIImage(systemName: "heart")
        let favImage = self.viewModel.isSaved(show: currentShow) ? UIImage(systemName: "heart.fill") : UIImage(systemName: "heart")
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: favImage, style: .plain, target: self, action: #selector(toggleFavorite))
    }
    
    @objc func toggleFavorite() {
        
        guard let currentShow = currentShow else {
            print("Not Show object was passed from previous VC")
            return
        }
//        self.viewModel.setFavorite(showId: currentShow.id)
        self.viewModel.save(show: currentShow)
        refreshFavStatus()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1 + (self.currentShow?.seasons?.count ?? 0)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        
        guard let currentShow = self.currentShow, let seasons = currentShow.seasons else {
            return 0
        }
        
        switch section {
        case 0:
            return 3
            
        case 1...seasons.count:
            
            return seasons[section-1].episodes?.count ?? 0
            
            
        default:
            return 0
        }
        
        
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let currentShow = self.currentShow else {
            return UIView()
        }
        
        if section > 0 {
            let seasonButton = UIButton(type: .system)
            seasonButton.setTitle("Season \(section) -  \(currentShow.seasons?[section-1].episodes?.count ?? 0) Episodes", for: .normal)
            seasonButton.backgroundColor = .darkGray
            seasonButton.setTitleColor(.white, for: .normal)
            seasonButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
            seasonButton.sizeToFit()
            seasonButton.addTarget(self, action: #selector(handleExpanseCollapse), for: .touchUpInside)
            seasonButton.tag = section
            return seasonButton
        }
        
        return UIView()
    }
    
    @objc func handleExpanseCollapse(button: UIButton) {
        print("Expand and collapse header")
        
        
        
        let section = button.tag
        
        var indexPaths = [IndexPath]()
        
        guard let currentShow = self.currentShow, let seasons = currentShow.seasons, let episodes = seasons[section-1].episodes else {
            return
        }
        
        
        
//      Append them to list to know which to expand / collapse
        for row in episodes.indices {
            let indexPath = IndexPath(row: row, section: section)
            indexPaths.append(indexPath)
        }
    
        
//        self.tableView.deleteRows(at: indexPaths, with: .fade)
            
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section > 0 {
            return 36
        }
        
        return 0
    }
        
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print(indexPath.section)
        
        guard let currentShow = currentShow, let seasons = currentShow.seasons else {
            
            return UITableViewCell()
        }
        
        guard let episodes = seasons[indexPath.section > 0 ? indexPath.section - 1 : 0].episodes else {
            return UITableViewCell()
        }
        
        

        
        switch (indexPath.section, indexPath.row) {
        case (0,0):
            let cell = tableView.dequeueReusableCell(withIdentifier: "PosterSummaryCell", for: indexPath) as! PosterSummaryCell
            
            
            cell.posterImageView.sd_setImage(with: URL(string: currentShow.imageUrls?.medium ?? ""), placeholderImage: UIImage(named: "poster-placeholder"), options: .delayPlaceholder, context: nil)
            
            cell.summaryLabel.attributedText = currentShow.summary?.htmlAttributedString()
            cell.summaryLabel.font = UIFont(name: "Helvetica Neue", size: 13.5)
            cell.summaryLabel.textColor = .label
            return cell
        
        case (0,1):
            let cell = UITableViewCell(style: .value1, reuseIdentifier: nil)
            cell.detailTextLabel?.text = currentShow.genres.joined(separator: ", ")
            cell.textLabel?.text = "Genre"
            return cell

        case (0,2):
            let cell = UITableViewCell(style: .value1, reuseIdentifier: nil)
            cell.textLabel?.text = currentShow.schedule?.days?.joined(separator: ", ")
            cell.detailTextLabel?.text = currentShow.schedule?.time
            return cell
            
        case (1...seasons.count,0..<episodes.count):
            
            let cell = UITableViewCell(style: .value1, reuseIdentifier: nil)
            cell.detailTextLabel?.text = "Episode \(episodes[indexPath.row].number ?? 0)"
            cell.textLabel?.text = episodes[indexPath.row].name
            return cell
            
            
        
            
        default:
            break
            
        }
        let cell = UITableViewCell()
        return cell

     
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        
        return UITableView.automaticDimension
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        guard let currentShow = currentShow, let seasons = currentShow.seasons, let episodes = seasons[indexPath.section > 0 ? indexPath.section - 1 : 0].episodes else {
            return
        }
        
        
        switch (indexPath.section, indexPath.row) {
            
        case (0, _) :
            return //First section, no interaction
            
        case (1...seasons.count, 0..<episodes.count):
            
            print("Show season \(indexPath.section), episode \(indexPath.row+1)")
            
            performSegue(withIdentifier: "EpisodeDetails", sender: indexPath)
            
            break;
            
        default:
            return
            
        }
        
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
        
        
        
        
        if segue.identifier == "EpisodeDetails" {
            
            guard let indexPath = sender as? IndexPath else {
                return
            }
            
            guard let currentShow = currentShow, let seasons = currentShow.seasons, let episodes = seasons[indexPath.section > 0 ? indexPath.section - 1 : 0].episodes else {
                return
            }
            
            let episodeVC = segue.destination as! EpisodeDetailsViewController
            episodeVC.episode = episodes[indexPath.row]
        }
        
        
    }
    
    

}
