//
//  ShowsIndexViewController.swift
//  JobsityTvTracker
//
//  Created by David Hsieh on 11/11/21.
//

import UIKit
import SDWebImage

class ShowsIndexViewController: UITableViewController {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var shows = [Show]()
    let viewModel = ShowsIndexViewModel()
    let activityView = UIActivityIndicatorView(style: .large)
    var isLoading = false
    var nextPage = 0
    var userDefaults = UserDefaults.standard
    
    private var observer: NSObjectProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.tintColor = .systemRed
        tabBarController?.tabBar.tintColor = .systemRed
        
        viewModel.delegate = self

        // Setup observer to launch authentication view everytime the app comes to foreground
        self.activityView.center = CGPoint(x: self.view.center.x, y: self.view.center.y-100)
        self.view.addSubview(self.activityView)
        
        if userDefaults.bool(forKey: "Authenticate") {
            observer = NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: .main, using: {
                [unowned self] notification in
                let authenticationVC = AuthenticationViewController()
                self.present(authenticationVC, animated: true, completion: {
                    
                })
            })
            
            let authenticationVC = AuthenticationViewController()
            self.present(authenticationVC, animated: true, completion: {
                
            })
            
        }
        
        
    }
    
    deinit {
        if let observer = observer {
            NotificationCenter.default.removeObserver(observer)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.changeViewBasedOnSegment()
    }
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        
        self.changeViewBasedOnSegment()
    }
    
    func changeViewBasedOnSegment() {
        switch (self.segmentedControl.selectedSegmentIndex) {
            
        case 0:
            self.nextPage = 0
            self.viewModel.getShowsOn(page: self.nextPage)
            
            break
            
        case 1:
            viewModel.getFavoriteShows()
            break
        case 2:
            self.promptSearchAlert()
            break
            
        default:
            break
            
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return shows.count > 0 ? 1 : 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return shows.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "showTitleCell", for: indexPath) as! ShowTitleTableviewCell
        
        let show = shows[indexPath.row]
        let imageUrl = URL(string: show.imageUrls?.medium ?? "" )
            
        let index = show.premiered?.index(show.premiered!.startIndex, offsetBy: 3)

        
        cell.showName.text = "\(show.name) (\(show.premiered?[...index!] ?? "" ))"
        
        cell.showImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "poster-placeholder"), options: .delayPlaceholder, context: nil)
                
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
         return 200
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDetails", sender: indexPath);
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch(segue.identifier) {
        case "showDetails":
            let showDetailsVC = segue.destination as! ShowDetailsViewController
            let indexPath = sender as! IndexPath
            showDetailsVC.currentShow = shows[indexPath.row]
            break
            
            
        default:
            return
        }
    }

    
    func promptSearchAlert() {
        
        
        
        let alertController = UIAlertController(title: "Search Show", message: "Type the name of the show you are looking for", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = "e.g. Expanse"
        }
        
        let confirmAction = UIAlertAction(title: "Search", style: .default, handler: { action in
            
            let input = alertController.textFields?[0].text
            
            
            
            self.viewModel.searchForShow(query: input ?? "")
            
            
                
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil);
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: {
            
        })
        
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.segmentedControl.selectedSegmentIndex != 0 {
            return
        }
        
        if self.nextPage == 0 || self.isLoading {
            return
        }
        
        let position = scrollView.contentOffset.y
        
        if position > self.tableView.contentSize.height-761 {
            viewModel.getShowsOn(page: nextPage)
            self.nextPage += 1
        }
        
        
    }
    


}
