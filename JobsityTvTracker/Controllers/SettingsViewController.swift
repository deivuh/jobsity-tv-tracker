//
//  SettingsViewController.swift
//  JobsityTvTracker
//
//  Created by David Hsieh on 15/11/21.
//

import UIKit
import LocalAuthentication

class SettingsViewController: UITableViewController {

    @IBOutlet weak var lockSwitchLabel: UILabel!
    @IBOutlet weak var lockSwitch: UISwitch!
    
    let userDefaults = UserDefaults.standard
    
    
    @IBAction func changedLockSetting(_ sender: UISwitch) {
        userDefaults.setValue(sender.isOn, forKey: "Authenticate")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Settings"
        
        lockSwitch.setOn((userDefaults.bool(forKey: "Authenticate") ), animated: true)
        var biometricTypeString = ""
        
        switch(LAContext().biometricType) {
        case .faceID:
            biometricTypeString = "Face ID"
            break
        case .touchID:
            biometricTypeString = "Touch ID"
            break
        default:
            biometricTypeString = ""
            break
        }
        
        lockSwitchLabel.text = "Lock with \(biometricTypeString) & Passcode"
        
        

    }
    
    @IBAction func clearData(_ sender: Any) {
        let alert = UIAlertController(title: "Are you sure?", message: "By doing this, all configuration and data (favorite shows) will be cleared from the app", preferredStyle:  .actionSheet)
        
        let confirmationAction = UIAlertAction(title: "Yes", style: .destructive, handler: { action in
            self.clearUserDefaults()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(confirmationAction)
        
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true)
    }
    
    
    func clearUserDefaults() {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
    }
}
